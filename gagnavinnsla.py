#!/usr/env/bin python

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Skilgreinum breytur og fylki
show = False
T = np.arange(23,34,1)
T = np.append(T, np.arange(36,50,2))
T = np.append(T, np.arange(55,80,5))
T = np.append(T, np.arange(100,300,20))
Hall = np.empty([T.size, 4], dtype = object)
Pauw = np.empty([T.size, 8], dtype = object)
Rs = np.zeros(T.size)
ns = np.empty([T.size, 4], dtype = object)
mu = np.empty([T.size, 4], dtype = object)
summa = np.zeros(T.size)
k = 1.3806e-23

# Lesum gögnin inn í fylki
for i in range(T.size):
    for j in range(12):
        dat = 'Gogn/ch' + str(j+1) + '_T' + str(T[i]) + 'gonybois.dat'
        if j < 4:
            Hall[i,j] = np.loadtxt(dat, delimiter = '\t', skiprows = 1)
        if j >= 4:
            Pauw[i,j-4] = np.loadtxt(dat, delimiter = '\t', skiprows = 1)

# Reiknum Rs úr Pauw mælingum
def getSheet(Pauw):
    Ra = (Pauw[1][3,4] + Pauw[0][3,4] + Pauw[7][3,4] + Pauw[6][3,4])/4
    Rb = (Pauw[2][3,4] + Pauw[5][3,4] + Pauw[4][3,4] + Pauw[3][3,4])/4
    err = 1.0
    z = 2*np.log(2)/(np.pi*(Ra+Rb))
    while err > 0.0005:
        y = 1/np.exp(np.pi*z*Ra) + 1/np.exp(np.pi*z*Rb)
        temp = z
        z = z - ((1 - y)/np.pi)/(Ra/np.exp(np.pi*z*Ra) + Rb/np.exp(np.pi*z*Rb))
        err = np.abs((z - temp)/z)
    Rs = 1/z
    return Rs

# Reiknum R_s fyrir öll hitastig
for i in range(T.size):
    p = Pauw[i,:]
    Rs[i] = getSheet(p)

# Plottum R_s sem fall af hitastigi
fig1, ax1 = plt.subplots()
ax1.scatter(T, Rs, label='$R_s$', marker = '+')
ax1.set(yscale = 'log', xlabel = 'T [K]', ylabel = '$R_s$ [$\Omega$]')
ax1.legend()
if show:
    fig1.show()
fig1.savefig('Graf1_Rs-T.png')

# Reiknum Hallspennusummu
def getSum(Hall):
    summa = 0
    for i in range(4):
        #H = Hall[i]
        summa = summa + np.sum(Hall[i][:2,3]) - np.sum(Hall[i][4:,3])
    return summa

# Reiknum summu fyrir öll hitastig
for i in range(T.size):
    summa[i] = getSum(Hall[i,:])

# Plottum summu sem fall af hitastigi
fig5, ax5 = plt.subplots()
ax5.scatter(T, summa)
ax5.plot([23,300],[0,0])
ax5.set(ylabel = '$V_C+V_D+V_E+V_F$ $[V]$', xlabel = 'T [K]')
if show:
    fig5.show()
fig5.savefig('Graf5_summa-T.png')
sample = 'p'
for i in range(summa.size):
    if summa[i] < 0:
        sample = 'n'
print('Sýnið er hálfleiðari af gerð ' + sample)

#input()

# Reiknum carrier density úr Hall mælingum
def getCarDens(Hall):
    q = 1.602e-19
    #Tökum bara gildi við segulsvið
    I = (np.sum(np.abs(Hall[:2,2])) + np.sum(np.abs(Hall[4:,2])))/6
    B = (np.sum(np.abs(Hall[:2,1])) + np.sum(np.abs(Hall[4:,1])))/6
    Vh = (np.sum(np.abs(Hall[:2,3])) + np.sum(np.abs(Hall[4:,3])))/6
    ns = I*B/(q*np.abs(Vh))*10**(-4)
    return ns

# Reiknum hleðsluberaþéttleika fyrir öll T
for i in range(4):
    for j in range(T.size):
        ns[j,i] = getCarDens(Hall[j,i])

# Plottum hleðsluberaþéttleika sem fall af hitastigi
fig2, ax2 = plt.subplots()
ax2.semilogy(T, ns[:,0])
ax2.semilogy(T, ns[:,1], label = '$I_{13}$', c = 'b', marker = '+')
ax2.semilogy(T, ns[:,2])
ax2.semilogy(T, ns[:,3], label = '$I_{24}$', c = 'r', marker = '+')
ax2.set(yscale = 'log', ylabel = '$n_s$ $[\si{\cm^{-2}}]$', xlabel = 'T [K]')
ax2.legend()
if show:
   fig2.show()
fig2.savefig('Graf2_T-ns.png')

# Hjálparfall til að reikna hreyfanleika
def hallHreyfi(ns, Rs):
    q = 1.602e-19
    mu = 1/(q*ns*Rs)*10**4
    return mu

## Reiknum hreyfanleika fyrir alla hleðsluberaþéttleika
for i in range(4):
    for j in range(T.size):
        mu[j,i] = hallHreyfi(ns[j,i], Rs[j])
##

## Plottum hreyfanleika sem fall af hitastigi
fig3, ax3 = plt.subplots()
ax3.scatter(T, mu[:,1], label = '$I_{13}$', marker = '+')
ax3.scatter(T, mu[:,3], label = '$I_{24}$', marker = '+')
ax3.set(xlabel = 'T [K]', ylabel = '$\mu$ $[\si{\cm^2}/V_s]$')
ax3.legend()
if show:
    fig3.show()
fig3.savefig('Graf3_T-mu.png')

# Plottum logrann af hleðsluberaþéttleikanum á móti 1/KbT, með T frá 23-40
fig4, ax4 = plt.subplots()
ax4.scatter(1/(k*T), np.log(ns[:,0].astype(np.float64)), marker = '+', label = '$\ln{n_s}$')
Tlo = T[0:13]
nslo = ns[0:13,0]
best, cov = np.polyfit(1/(Tlo*k), np.log(nslo.astype(np.float64)), deg = 1, cov = True)
ax4.plot(1/(k*T), best[1] + best[0]*(1/(k*T)), label = 'Besta lína', c = 'g')
print('Besta lína: ' + str(best[0]) + 'x' + ' + ' + str(best[1]))
ovissa = np.sqrt(np.diag(cov))
print('Með óvissu: ' + str(ovissa[0]))
print('Sem gefur: E = ' + '(' + str(-best[0]/1.602e-19) + ' +- ' + str(ovissa[0]/1.602e-19) + ')' + 'eV')
ax4.set(xlabel = '$(k_BT)^{-1}$ [$\si{\joule^{-1}}$]', ylabel = '$\ln(n_s)$')
ax4.legend()
if show:
    fig4.show()
fig4.savefig('Graf4_lnn-kbt.png')
if show:
    input()
